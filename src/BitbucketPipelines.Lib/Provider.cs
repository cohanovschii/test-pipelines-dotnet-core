﻿using System;

namespace BitbucketPipelines.Lib
{
    public class Provider
    {
        public int Sum(int a, int b) => a + b;

        public int Substract(int a, int b) => a - b;
    }
}
